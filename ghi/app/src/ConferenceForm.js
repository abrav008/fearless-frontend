import React, { useEffect, useState } from "react";


function ConferenceForm(props) {
    const [locations, setLocations] = useState([])
    const [name, setName] = useState('');
    const [starts, setStarts] = useState('');
    const [ends, setEnds] = useState('');
    const [description, setDescription] = useState('');
    const [maxPresentations, setMaxPresentations] = useState('');
    const [maxAttendees, setMaxAttendees] = useState('');
    const [location, setLocation] = useState('');

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value)
    }

    const handleStartsChange = event => {
        const value = event.target.value;
        setStarts(value);
    }

    const handleEndsChange = event => {
        const value = event.target.value;
        setEnds(value)
    }

    const handleDescriptionChange = event => {
        const value = event.target.value;
        setDescription(value)
    }

    const handleMaxPresentationsChange = event => {
        const value = event.target.value;
        setMaxPresentations(value)
    }

    const handleMaxAttendeesChange = event => {
        const value = event.target.value;
        setMaxAttendees(value);
    }

    const handleLocationChange = event => {
        const value = event.target.value;
        setLocation(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {}

        data.name = name;
        data.starts = starts;
        data.ends = ends;
        data.description = description;
        data.max_presentations = maxPresentations;
        data.max_attendees = maxAttendees;
        data.location = location;

        console.log(data);

        const locationUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };

        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
          const newLocation = await response.json();
          console.log(newLocation);

          setName('');
          setStarts('');
          setEnds('');
          setDescription('');
          setMaxPresentations('');
          setMaxAttendees('')
          setLocation('')
        }
      }

    const fetchData = async () => {
        const url = 'http://localhost:8000/api/locations/';
        const response = await fetch(url);

        if(response.ok){
            const data = await response.json();
            setLocations(data.locations)
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">
                <div className="form-floating mb-3">
                    <input value={name} onChange={handleNameChange} placeholder="Name" required type="text" id="name" className="form-control" name="name"/>
                    <label htmlFor="name">Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={starts} onChange={handleStartsChange} placeholder="starts" required type="date" id="starts" className="form-control" name="starts"/>
                    <label htmlFor="starts">Starts</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={ends} onChange={handleEndsChange} placeholder="ends" required type="date" id="ends" className="form-control" name="ends"/>
                    <label htmlFor="ends">Ends</label>
                </div>
                <div className="mb-3">
                    <label htmlFor="description">Description</label>
                    <textarea value={description} onChange={handleDescriptionChange} placeholder="Enter description" required type="textarea" className="form-control" name="description" rows="3"></textarea>
                </div>
                <div className="form-floating mb-3">
                    <input value={maxPresentations} onChange={handleMaxPresentationsChange} placeholder="Max presentations" required type="number" id="max_presentations" className="form-control" name="max_presentations"/>
                    <label htmlFor="max_presentations">Max presentations</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={maxAttendees} onChange={handleMaxAttendeesChange} placeholder="Max attendees" required type="number" id="max_attendees" className="form-control" name="max_attendees"/>
                    <label htmlFor="max_attendees">Max attendees</label>
                </div>
                <div className="mb-3">
                <select value={location} onChange={handleLocationChange} required name="location" id="location" className="form-select">
                    <option value="">Choose a Location</option>
                    {locations.map(location => {
                        return (
                        <option key={location.name} value={location.id}>
                            {location.name}
                        </option>
                        );
                    })}
                </select>
                </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    )
}

export default ConferenceForm
