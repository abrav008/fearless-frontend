window.addEventListener('DOMContentLoaded', async () =>{
    const url = 'http://localhost:8000/api/locations/';
    const response = await fetch(url);

    if(response.ok){
        const data = await response.json();

        const selectTag = document.querySelector('#location')
        const locations = data.locations

        locations.forEach((location) => {
            let locationOption = document.createElement('option');

            locationOption.value = location.id;
            locationOption.innerHTML = location.name;

            selectTag.append(locationOption);
        })
    }

    const formTag = document.getElementById('create-conference-form');
    formTag.addEventListener('submit', async event => {
        event.preventDefault()

        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));

        const locationUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(locationUrl, fetchConfig);
        if(response.ok) {
            formTag.reset();
            const newAttendee = await response.json();
            console.log(newAttendee)
        }
    })
})
