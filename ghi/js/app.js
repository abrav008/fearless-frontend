function createCard(name, description, pictureUrl, startDate, endDate, location) {
    return `
      <div class="card shadow mb-3 bg-white rounded">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">${startDate} - ${endDate}</div>
      </div>
    `;
  }


function createError(error){
    `
    <div class="alert alert-primary" role="alert">
    A simple primary alert—check it out! ${error}
    </div>
    `
}


window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';
    try{
        const response = await fetch(url);
        if(!response.ok){
            createError(error);
        } else {
            const data = await response.json();


            let count = 0
            const columns = document.querySelectorAll('.col');
            for (let conference of data.conferences) {
              const detailUrl = `http://localhost:8000${conference.href}`;
              const detailResponse = await fetch(detailUrl);
              if (detailResponse.ok) {
                const details = await detailResponse.json();
                const title = details.conference.name;
                const description = details.conference.description;
                const pictureUrl = details.conference.location.picture_url;
                const location = details.conference.location.name

                let startDate = new Date(details.conference.starts);
                let endDate = new Date(details.conference.ends);
                startDate = startDate.toLocaleDateString();
                endDate = endDate.toLocaleDateString();

                const html = createCard(title, description, pictureUrl, startDate, endDate, location);
                const column = columns[count % columns.length];
                column.innerHTML += html;
                if (count===0)  {
                  const placeHolder = document.getElementById("card-placeholder")
                  placeHolder.classList.add("d-none")
                }
                count += 1
              }
            }

          }
    } catch(error) {
        createError(error)
    }
});
